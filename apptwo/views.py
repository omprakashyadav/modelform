from django.shortcuts import render
# from django.http import HttpResponse
from . import forms
# Create your views here.
def index(request):
    return render(request, 'protwo/index.html')






def user_form(request):
    form = forms.UserForm()
    if request.method =='POST':
        form = forms.UserForm(request.POST)
        if form.isvalid():
            print("validation successs")
            print("name :" +form.cleaned_data['name'])
            print("email :" +form.cleaned_data['email'])
            print(" password :" +form.cleaned_data['password'])
            print("text :" +form.cleaned_data['text'])
    return render(request, 'protwo/user_form.html', {'form': form})

def user_model_form(request):
    form = forms.UserModelForm()
    if request.method =='POST':
        form = forms.UserModelForm(request.POST)
        if form.isvalid():
           form.save(commit=True)
           return index(request)
        else:
            print("invalid page return")   
    return render(request, 'protwo/user_form_model.html', {'form': form})



def card_view(request):
    return render(request, 'protwo/card.html')

def category_view(request):
    return render(request, 'protwo/category.html')


def order_view(request):
    return render(request, '/protwo/order.html')

def product_view(request):
    return render(request, 'protwo/product.html')

