
from django.urls import path
from apptwo import views



urlpatterns = [
    path('product', views.product_view, name='product'),
    path('card', views.card_view, name='card'),
    path('order', views.order_view, name='order'),
    path('category' , views.category_view, name='category'),
    # path('use' , views.user_form, name='use')
]
